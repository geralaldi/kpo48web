<!-- 
	Koneksi Database
-->
<?php
	require_once('db_connect.php');
	$db = new DB_CONNECT();
?>
<!DOCTYPE XHTML>

<html>
	<head>
		<title>KPO48 - Kepo Your Friend But Be Strong</title>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!-- Bootstrap -->
	    <link href="bootstrap/css/bootstrap_cosmo.css" rel="stylesheet" media="screen">
	  	<script src="bootstrap/js/jquery.min.js"></script>
	    <script src="bootstrap/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="../koplak.php">KPO48</a>
				</div>
				<div class="navbar-collapse collapse navbar-responsive-collapse">
					<ul class="nav navbar-nav">
						<li class="dropdown disabled"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Prodi<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li role="presentation" class="dropdown-header">Fakultas Farmasi</li>
								<li><a href="#">Farmasi</a></li>
								<li class="divider"></li>
								<li role="presentation" class="dropdown-header">Fakultas MIPA</li>
								<li><a href="#">Elektonika dan Instrumentasi</a></li>
								<li><a href="#">Fisika</a></li>
								<li><a href="#">Geofisika</a></li>
								<li><a href="#">Ilmu Komputer</a></li>
								<li><a href="#">Kimia</a></li>
								<li><a href="#">Matematika</a></li>
								<li><a href="#">Statistika</a></li>
								<li class="divider"></li>
								<li role="presentation" class="dropdown-header">Fakultas Teknik</li>
								<li><a href="#">Teknik Industri</a></li>
							</ul>
						</li>
						<li></li>
					</ul>
				</div>
			</div>
		</div>	
		<div class="container">
			<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<div class="container center">
					<h3 align="center">Kepo Your Friend, But Be Strong</h3>
					<form method="post" action="<?php $_SERVER['PHP_SELF']?>">
						<input type="text" id="data" class="form-control" name="data" placeholder="Masukkan NIU atau Nama Mahasiswa">
						<div>&nbsp;</div>
						<input class="btn btn-primary pull-right" type="submit" value="Submit">
					</form>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<table class="table table-striped table-bordered">
						<th scope="col"><div align="center"><b>Nama</b></div></th>
						<th scope="col"><div align="center"><b>Prodi</b></div></th>
	          <th scope="col"><div align="center"><b>IP</b></div></th>
	          <th scope="col"><div align="center"><b>KRS</b></div></th>
					<?php
						if (isset($_POST['data']) && $_POST['data'] != '') {
							$data = $_POST['data'];
							if (is_numeric($data)) {
								$query = mysql_query("SELECT * FROM mahasiswa WHERE niu = $data");
								while($row = mysql_fetch_array($query)) {
										$nama = $row['nama'];
										$prodi = $row['prodi'];

										if ($prodi == 'NTo') {
											$prod = 'Ilmu Komputer';
										}
										else if ($prodi == 'NjM') {
											$prod = 'Elektronika dan Instrumentasi';
										}
										else if ($prodi == 'OA') {
											$prod = 'Farmasi';
										}
										else if ($prodi == 'OjY') {
											$prod = 'Teknik Industri';
										}
										else if ($prodi == 'NTc') {
											$prod = 'Fisika';
										}
										else if ($prodi == 'NTw') {
											$prod = 'Geofisika';
										}
										else if ($prodi == 'NTg') {
											$prod = 'Kimia';
										}
										else if ($prodi == 'NTk') {
											$prod = 'Matematika';
										}
										else if ($prodi == 'NTs') {
											$prod = 'Statistika';
										}

										for($i=0;$i<6;$i++){
											$niu[$i]=chr(ord(substr($row['niu'],$i,1))+3);
										}
										$niu0 = implode("",$niu);
										$niul[0] = base64_encode(substr($niu0,0,3));
										$niul[1] = base64_encode(substr($niu0,3,3));
										$niu_enc = implode($niul);
										echo "
										<tr>
										<td>
											<div align='center'>$nama</div>
										</td>
										<td>
											<div align='center'>$prod</div>
										</td>
										<td></td>
										<td align='center'><a target='blank' href='https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=$niu_enc&prodi=NTo'>Link</a></td>
										</tr>
										";
								}
							}
							else {
								$query = mysql_query("SELECT * FROM mahasiswa WHERE nama LIKE  '%$data%'");
								while($row = mysql_fetch_array($query)) {
										$nama = $row['nama'];
										$prodi = $row['prodi'];

										if ($prodi == 'NTo') {
											$prod = 'Ilmu Komputer';
										}
										else if ($prodi == 'NjM') {
											$prod = 'Elektronika dan Instrumentasi';
										}
										else if ($prodi == 'OA') {
											$prod = 'Farmasi';
										}
										else if ($prodi == 'OjY') {
											$prod = 'Teknik Industri';
										}
										else if ($prodi == 'NTc') {
											$prod = 'Fisika';
										}
										else if ($prodi == 'NTw') {
											$prod = 'Geofisika';
										}
										else if ($prodi == 'NTg') {
											$prod = 'Kimia';
										}
										else if ($prodi == 'NTk') {
											$prod = 'Matematika';
										}
										else if ($prodi == 'NTs') {
											$prod = 'Statistika';
										}

										for($i=0;$i<6;$i++){
											$niu[$i]=chr(ord(substr($row['niu'],$i,1))+3);
										}
										$niu0 = implode("",$niu);
										$niul[0] = base64_encode(substr($niu0,0,3));
										$niul[1] = base64_encode(substr($niu0,3,3));
										$niu_enc = implode($niul);

										echo "
										<tr>
										<td>
											<div align='center'>$nama</div>
										</td>
										<td>
											<div align='center'>$prod</div>
										</td>
										<td></td>
										<td align='center'><a target='blank' href='https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=$niu_enc&prodi=$prodi'>Link</a></td>
										</tr>
										";
								}
							}
						}
					?>	
					</table>
				</div>
			</div>
			<div class="col-lg-2"></div>
		</div>			
	</body>
</html>
