<?php
mysql_connect("mysql14.000webhost.com","a8063929_kpo48","399652a");
mysql_select_db("a8063929_koplak");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Kepo Site - Be Strong</title>
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #42413C;
	margin: 0;
	padding: 0;
	color: #000;
	background-color: #003;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}
/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this fixed width container surrounds the other divs ~~ */
.container {
	width: 960px;
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout */
}

/* ~~ the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo ~~ */
.header {
	background: #ADB96E;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/

.content {

	padding: 10px 0;
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #CCC49F;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}
body,td,th {
	font-family: Tahoma, Geneva, sans-serif;
}
-->

</style>
<link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<script type="text/javascript">
	function alertBeta() {
		alert("Visit our new beta site, click on Beta Site link");
	}
</script>
<script type="text/javascript" src="scripts/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="scripts/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript">
  $(window).load(function() {
    $('#slider').nivoSlider({directionNavHide:false});
  });
</script>
</head>

<body onload="alertBeta()">
<div class="container">
  <div class="header">
    <div align="center"><a href="http://kpo48.webege.com"><img src="header-portal-UGM.gif" width="960" height="76"></div></a>
    <!-- end .header -->
  </div>
  <div class="content">
    <h1 align="center"><a href="beta/index.php"><strong>Beta Site</strong></a></h1>
    <div id="wrapper">
      <div id="slider-wrapper">
				<div id="slider" class="nivoSlider">
          <img src="images/toystory.jpg" alt="" />
          <img src="images/up.jpg" alt="" />
          <img src="images/walle.jpg" alt="" />
          <img src="images/nemo.jpg" alt="" />
          <img src="images/walle.jpg" alt="" />
        </div>    
      <p>&nbsp;</p>
      </div>
    </div>
    <center><form name="form1" method="get" action="<?php $_SERVER['PHP_SELF']?>">
      <p><b>Jangan lupa untuk Login ke <a href="https://akademika.ugm.ac.id">Portal Akademika UGM</a></b></p>
      <p>&nbsp;</p>
      <p>
        <label for="angkatan">Angkatan</label>
        <select name="angkatan" id="angkatan">
          <option value=""></option>
          <option>2002</option>
          <option>2003</option>
          <option>2004</option>
          <option>2005</option>
          <option>2006</option>
          <option>2007</option>
          <option>2008</option>
          <option>2009</option>
          <option>2010</option>
          <option>2011</option>
          <option>2012</option>
          <option>2013</option>
        </select>
        <label for="prodi">Program Studi </label>
        <select name="prodi" id="prodi">
          <option value=""></option>
          <option value="Elektronika dan Instrumentasi">Elektronika dan Instrumentasi</option>
          <option value="Farmasi">Farmasi</option>
          <option value="Ilmu Komputer">Ilmu Komputer</option>
          <option value="Matematika">Matematika</option>
          <option value="Teknik Industri">Teknik Industri</option>
          <option value="Statistika">Statistika</option>
          <option value="Fisika">Fisika</option>
          <option value="Geofisika">Geofisika</option>
          <option value="Kimia">Kimia</option>
        </select> 
        <input type="submit" name="Buka" id="Buka" value="Buka">
      </p>
      <p>&nbsp; </p> 
      <p><strong>
					<b>
						<?php 
							if(isset($_GET['prodi'])) {
							  echo ($_GET['prodi']);
							}
						?> 
					</b>
				</strong>
			</p>
      <p>&nbsp;</p>
			<?php
			if(isset($_GET['angkatan']) and isset($_GET['prodi']) and (($_GET['prodi'])!="")) { ?>
				<table width="600" border="1" align="center">
			        <tr bgcolor="#CCCCCC">
			          <th scope="col"><div align="center"><b>NIU</b></div></th>
			          <th scope="col"><div align="center"><b>Nama</b></div></th>
			          <th scope="col"><div align="center"><b>Angkatan</b></div></th>
			          <th scope="col"><div align="center"><b>Link</b></div></th>
			        </tr>
				<?php
				$tm = "where angkatan = $_GET[angkatan]";
				if($_GET['prodi'] == "Ilmu Komputer") {
				$query = mysql_query("select * from ilmu_komputer $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['nama']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['angkatan']?></div>
						</td>
						<td>
							<div align="center">
								<?php
									for($i=0;$i<6;$i++) {
										$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
									}
									$niu0 = implode("",$niu);
									$niul[0] = base64_encode(substr($niu0,0,3));
									$niul[1] = base64_encode(substr($niu0,3,3));
									$niu_enc = implode($niul);
								?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTo=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTo=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=NTo=" target="_blank">KRS</a>
					    </div>
					  </td>
				</tr>
				<?php
			  	}
			  }
				else if($_GET['prodi'] == "Elektronika dan Instrumentasi") {
				$query = mysql_query("select * from elektronika_instrumentasi $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['nama']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['angkatan']?>
						    </div></td>
						<td>
							<div align="center">
							  <?php
							for($i=0;$i<6;$i++){
								$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
							}
							$niu0 = implode("",$niu);
							$niul[0] = base64_encode(substr($niu0,0,3));
							$niul[1] = base64_encode(substr($niu0,3,3));
							$niu_enc = implode($niul);
							?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=NjM=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=NjM=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=NjM=" target="_blank">KRS</a>
						    </div></td>
					</tr>
				<?php
					}		
				}
				else if($_GET['prodi'] == "Matematika") {
				$query = mysql_query("select * from matematika $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['nama']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['angkatan']?>
						    </div></td>
						<td>
							<div align="center">
							<?php
							for($i=0;$i<6;$i++){
								$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
							}
							$niu0 = implode("",$niu);
							$niul[0] = base64_encode(substr($niu0,0,3));
							$niul[1] = base64_encode(substr($niu0,3,3));
							$niu_enc = implode($niul);
							?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTk=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTk=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=NTk=" target="_blank">KRS</a>
						    </div></td>
					</tr> 
				<?php
					}
				}
				else if($_GET['prodi'] == "Teknik Industri") {
				$query = mysql_query("select * from teknik_industri $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['nama']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['angkatan']?>
						    </div></td>
						<td>
							<div align="center">
							  <?php
							for($i=0;$i<6;$i++){
								$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
							}
							$niu0 = implode("",$niu);
							$niul[0] = base64_encode(substr($niu0,0,3));
							$niul[1] = base64_encode(substr($niu0,3,3));
							$niu_enc = implode($niul);
							?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=OjY=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=OjY=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=OjY=" target="_blank">KRS</a>
						    </div></td>
					</tr> 
				<?php
					}
				}
				else if($_GET['prodi'] == "Farmasi") {
				$query = mysql_query("select * from farmasi $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['nama']?>
						    </div></td>
						<td>
							<div align="center"><?php echo $data['angkatan']?>
						    </div></td>
						<td>
							<div align="center">
							  <?php
							for($i=0;$i<6;$i++){
								$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
							}
							$niu0 = implode("",$niu);
							$niul[0] = base64_encode(substr($niu0,0,3));
							$niul[1] = base64_encode(substr($niu0,3,3));
							$niu_enc = implode($niul);
							?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=OA=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=OA=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=OA=" target="_blank">KRS</a>
						    </div></td>
					</tr> 
				<?php
					}
				}
				else if($_GET['prodi'] == "Statistika") {
				$query = mysql_query("select * from statistika $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['nama']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['angkatan']?></div>
						</td>
						<td>
							<div align="center">
								<?php
									for($i=0;$i<6;$i++) {
										$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
									}
									$niu0 = implode("",$niu);
									$niul[0] = base64_encode(substr($niu0,0,3));
									$niul[1] = base64_encode(substr($niu0,3,3));
									$niu_enc = implode($niul);
								?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTs=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTs=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=NTs=" target="_blank">KRS</a>
					    </div>
					  </td>
				</tr>
				<?php
			  	}
			  }
				else if($_GET['prodi'] == "Fisika") {
				$query = mysql_query("select * from fisika $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['nama']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['angkatan']?></div>
						</td>
						<td>
							<div align="center">
								<?php
									for($i=0;$i<6;$i++) {
										$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
									}
									$niu0 = implode("",$niu);
									$niul[0] = base64_encode(substr($niu0,0,3));
									$niul[1] = base64_encode(substr($niu0,3,3));
									$niu_enc = implode($niul);
								?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTc=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTc=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=NTc=" target="_blank">KRS</a>
					    </div>
					  </td>
				</tr>
				<?php
			  	}
			  }
				else if($_GET['prodi'] == "Geofisika") {
				$query = mysql_query("select * from geofisika $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['nama']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['angkatan']?></div>
						</td>
						<td>
							<div align="center">
								<?php
									for($i=0;$i<6;$i++) {
										$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
									}
									$niu0 = implode("",$niu);
									$niul[0] = base64_encode(substr($niu0,0,3));
									$niul[1] = base64_encode(substr($niu0,3,3));
									$niu_enc = implode($niul);
								?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTw=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTw=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=NTw=" target="_blank">KRS</a>
					    </div>
					  </td>
				</tr>
				<?php
			  	}
			  }
				else if($_GET['prodi'] == "Kimia") {
				$query = mysql_query("select * from kimia $tm");
				while($data=mysql_fetch_array($query)){
				?>
					<tr>
						<td>
							<div align="center"><?php echo $data['niu']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['nama']?></div>
						</td>
						<td>
							<div align="center"><?php echo $data['angkatan']?></div>
						</td>
						<td>
							<div align="center">
								<?php
									for($i=0;$i<6;$i++) {
										$niu[$i]=chr(ord(substr($data['niu'],$i,1))+3);
									}
									$niu0 = implode("",$niu);
									$niul[0] = base64_encode(substr($niu0,0,3));
									$niul[1] = base64_encode(substr($niu0,3,3));
									$niu_enc = implode($niul);
								?>
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZid3VkcXZmdWxzdw==&amp;pAct=c3VscXc=&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTg=" target="_blank">Transkip</a> | 
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZidWhzcnV3&amp;pSub=ZGZkZ2hwbGZidWhzcnV3&amp;pAct=eWxoeg==&amp;niu=<?php echo $niu_enc?>&amp;prodi=NTg=" target="_blank">KHS</a> |
							  <a href="https://akademika.ugm.ac.id/index.php?pModule=ZGZkZ2hwbGZic29kcQ==&pSub=ZGZkZ2hwbGZic29kcQ==&pAct=c3VscXc=&niu=<?php echo $niu_enc?>&amp;prodi=NTg=" target="_blank">KRS</a>
					    </div>
					  </td>
				</tr>
				<?php
			  	}
			  }
			}
			?>
      </table>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </form></center>
    <p>&nbsp;</p>
		<center><p><b>Supported by</b></p></center>
    <p>&nbsp;</p>
		<center><!-- Twitter Code -->
		<a class="twitter-timeline" href="https://twitter.com/officialJKT48" data-widget-id="299128627650494464">Tweets by @officialJKT48</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</center>
    
		<center><!-- Start counter code -->
		<a href="http://www.roulette-vs-blackjack.dk" target="blank" >
		<img alt="Website counter" hspace="0" vspace="0" border="0" src="http://www.amazingcounters.info/5826124-627BEE548E288584FE6DE89777A5948D/counter.img?theme=3&digits=7&siteId=6"/>
		</a>
		<noscript><br/><a href="http://www.roulette-vs-blackjack.dk">www.roulette-vs-blackjack.dk</a><br>The following text will not be seen after you upload your website, please keep it in order to retain your counter functionality <br> <a href="http://cinamuse.wikispaces.com/movies+on+the+internet+at+cinamuse" target="_blank">cinamuse.com</a></noscript>
		</center>
		<!-- End counter code -->
		<p>&nbsp;</p>
	<!-- end .content -->
		</div>
		<div class="footer">
		  <p align="center"><strong><b>Copyright &copy; 2012 - Kepo Banget Sih :3</b></strong></p>
		    <!-- end .footer -->
		</div>
		  <!-- end .container -->
		</div>
	</body>
</html>						